<?php
use Migrations\AbstractMigration;

class ApiMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        /**
         * 
         * 
         * User Table
         */
        $table = $this->table('users');
            $table->addColumn('username', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false
            ])
            ->addColumn('confirm_password', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false
            ])
            ->addColumn('firstname', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false
            ])
            ->addColumn('lastname', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false
            ])
            ->addColumn('birthday', 'timestamp', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('location', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => true
            ])
            ->addColumn('city', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => true
            ])
            ->addColumn('email', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false
            ])
            ->addColumn('description', 'string', [
                'limit' => 140,
                'default' => null,
                'null' => true
            ])
            ->addColumn('image', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => true
            ])
            ->addColumn('activation', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => false
            ])
            ->addColumn('status', 'bit', [
                'limit' => 1,
                'default' => 0,
                'null' => false
            ])
            ->addColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
        ->save();

        /**
         * 
         * 
         * Tweet Table
         */
        $table = $this->table('tweets');
            $table->addColumn('user_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false,
            ])
            ->addColumn('content', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => true
            ])
            ->addColumn('picture', 'string', [
                'limit' => 100,
                'default' => null,
                'null' => true
            ])
            ->addColumn('retweet_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => true,
            ])
            ->addColumn('retweet_user_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => true,
            ])
            ->addColumn('retweet_user_content', 'integer', [
                'limit' => 100,
                'default' => null,
                'null' => true,
            ])
            ->addColumn('retweet_user_picture', 'integer', [
                'limit' => 100,
                'default' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addColumn('deleted', 'integer', [
                'default' => 0,
                'null' => false
            ])
            ->addForeignKey('user_id', 'users', 'id')
        ->save();

        /**
         * 
         * 
         * Likes Table
         */
        $table = $this->table('likes');
            $table->addColumn('user_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false,
            ])
            ->addColumn('tweet_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false
            ])
            ->addColumn('is_liked', 'integer', [
                'limit' => 1,
                'default' => 0,
                'null' => false
            ])
            ->addForeignKey('user_id', 'users', 'id')
        ->save();

        /**
         * 
         * 
         * Follower controller
         */
        $table = $this->table('followers');
            $table->addColumn('user_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false,
            ])
            ->addColumn('following_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false
            ])
            ->addColumn('status', 'integer', [
                'limit' => 1,
                'default' => 0,
                'null' => false
            ])
            ->addColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addForeignKey('user_id', 'users', 'id')
            ->addForeignKey('following_id', 'users', 'id')
        ->save();

        /**
         * 
         * 
         * Comments Table
         */
        $table = $this->table('comments');
            $table->addColumn('user_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false,
            ])
            ->addColumn('tweet_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => false
            ])
            ->addColumn('comment', 'string', [
                'limit' => 140,
                'default' => null,
                'null' => false
            ])
            ->addColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ])
            ->addForeignKey('user_id', 'users', 'id')
            ->addForeignKey('tweet_id', 'tweets', 'id')
        ->save();
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\AddUserForm;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Initialize method
     * @return void
     */
    public function initialize()
    {
        $this->loadComponent('CommonResponses');
        $this->loadComponent('Validator');
    }


    public function addUser() {
        if ($this->request->is('post')) {
            $result = [];

            $data = $this->request->getData();
    
            if (!$this->Validator->validate($data, 'AddUserForm')) {
                $error = $this->Validator->errors();
                return $this->CommonResponses->validationError($error);
            }
    
            if (!empty($data)) {
                return $this->CommonResponses->success([$data]);
            }

            return $this->CommonResponses->NotFound();

        }
        return $this->CommonResponses->methodNotAllowed();
    }

    
    public function checkUser() {

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            // if (!$this->Validator->validate($data, 'CheckUserForm')) {
            //     $error = $this->Validator->errors();
            //     return $this->CommonResponses->validationError($error);
            // }

            $username = $data['login_username'];

            $user = $this->Users->find()
                ->where([
                    'Users.username' => $username,
                    'Users.deleted' => 0
                ])
                ->toArray();

            if(!empty($user)) {
                return $this->CommonResponses->success([$user]);
            }
            return $this->CommonResponses->NotFound();
        }
    }

    public function updateActivation() {
        $activation = $this->request->getParam('activation');

        $data = $this->Users->find()
            ->select([
                'username',
                'password',
                'firstname',
                'lastname',
                'email',
                'activation',
                'status'
            ])
            ->Where([
                'Users.activation' => $activation
            ])
            ->toArray();
        //pr($data); die;

        // if (!empty($data)) {

        // }

        // $updatedUser = Collection($data)->map(function ($value) {

        // })
    }
}


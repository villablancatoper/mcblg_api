<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;

/**
 * Responses component
 */
class CommonResponsesComponent extends Component
{
    /**
     * setResponseBody private Method
     * This creates a response blueprint
     * @param array $body response body
     * @return object response
     */
    private function responseBody($body)
    {
        $response = $this->response->withHeader('Content-Type', 'application/json')
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Max-Age', '300')
            ->withHeader('Pragma', 'no-cache')
            ->withStatus($this->code)
            ->withStringBody(json_encode($body, JSON_UNESCAPED_UNICODE));

           return $response;
    }


    public function success($entity) {

        $body = [
            'status' => 200,
            'id' => 'SUCCESS',
            'message' => _('Success'),
            'data' => $entity
        ];
        $this->code = 200;

        return $this->responseBody($body);
    }

    public function notFound()
    {
        $body = [
            'status_code' => 404,
            'message_id' => 'NOT_FOUND',
            'message' => __('Not Found')
        ];
        $this->code = 404;

        return $this->responseBody($body);
    }


    public function validationError($error)
    {
        $body = [
            'status' => 400,
            'id' => 'INVALID_PARAMETERS',
            'message' => __('Invalid Parameters'),
            'data' => $this->errorFormat($error)
        ];
        $this->code = 400;

        return $this->responseBody($body);
    }


    private function errorFormat($errors)
    {
        $errorMethod = [];
        if (is_array($errors)) {
            foreach ($errors as $field => $message) {
                $errorMethod = array_keys($message);

                $errorMessage[] = [
                    'field_name' => $field,
                    'id' => $message[$errorMethod[0]],
                    'message' => __($message[$errorMethod[0]])
                ];
            }
        } else {
            $errorMessage = [
                'message' => $errors
            ];
        }

        return $errorMessage;
    }

    public function methodNotAllowed()
    {
        $body = [
            'status_code' => 405,
            'message_id' => 'METHOD_NOT_ALLOWED',
            'message' => __('Method Not Allowed')
        ];
        $this->code = 405;

        return $this->responseBody($body);
    }
}

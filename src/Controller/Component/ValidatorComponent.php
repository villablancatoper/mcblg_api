<?php
namespace App\Controller\Component;

use Cake\Controller\UsersController;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Validate component
 */
class ValidatorComponent extends Component
{

    public $components = ['CommonResponses'];

    public function validate($params, $form)
    {
        $params = $params;

        $class = '\App\Form\\' . $form;
        $object = new $class;

        $validator = $object->getValidator();

        $fields = [];
        $iterators = $validator->getIterator();

        while ($iterators->valid()) {
            $fields[] = $iterators->key();
            $iterators->next();
        }


        foreach ($fields as $field) {
            $validate[$field] = isset($params[$field]) ? $params[$field] : null;
        }

        if (!$object->validate($validate)) {
            $this->errors = $object->errors();

            return false;
        }

        return true;
    }
    /**
     * Function to return error messages
     *
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }
}

<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class LoginPasswordValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('login_password')
            ->notEmpty('login_password', 'PASSWORD_EMPTY')
            ->alphaNumeric('login_password', 'PASSWORD_ALPHANUMERIC')
            ->requirePresence('login_password', 'PASSWORD_REQUIRED');

        return $validator;
    }
}

<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class ConfirmPasswordValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('confirm_password')
            ->notEmpty('confirm_password', 'CONFIRM_PASSWORD_EMPTY')
            ->alphaNumeric('confirm_password', 'CONFIRM_PASSWORD_ALPHANUMERIC')
            ->requirePresence('confirm_password', 'CONFIRM_PASSWORD_REQUIRED')
            ->maxLength('confirm_password', 100, 'CONFIRM_PASSWORD_MAXIMUM_LENGTH')
            ->minLength('confirm_password', 5, 'CONFIRM_PASSWORD_MINIMUM_LENGTH')
            ->add('confirm_password', 'custom', [
                'rule' => function ($value, $context) {
                    if (isset($context['data']['password']) && $value == $context['data']['password']) {
                        return true;
                    }
                    return false;
                },
                'message' => 'CONFIRM_PASSWORD_MISMATCH',
            ]);

        return $validator;
    }
}

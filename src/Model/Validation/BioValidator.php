<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class BioValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('bio')
            ->notEmpty('bio', 'BIO_EMPTY')
            ->requirePresence('bio', 'BIO_REQUIRED')
            ->maxLength('bio', 100, 'BIO_MAXIMUM_LENGTH');

        return $validator;
    }
}

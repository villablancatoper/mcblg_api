<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class CityValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('city')
            ->maxLength('firstname', 100, 'FIRSTNAME_MAXIMUM_LENGTH')
            ->add('city', 'custom', [
                'rule' => function ($value) {
                    return $match = preg_match('/^[a-zA-Z ]*$/i', $value) ? true : false;
                },
                'message' => 'ALPHANUM_SPACES',
        ]);

        return $validator;
    }
}

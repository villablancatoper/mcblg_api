<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class NameValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('firstname')
            ->notEmpty('firstname', 'FIRSTNAME_EMPTY')
            ->requirePresence('firstname', 'FIRSTNAME_REQUIRED')
            ->maxLength('firstname', 100, 'FIRSTNAME_MAXIMUM_LENGTH')
            ->minLength('firstname', 2, 'FIRSTNAME_MINIMUM_LENGTH')
            ->add('firstname', 'custom', [
                'rule' => function ($value) {
                    $match = preg_match('/^[a-zA-Z ]*$/i', $value) ? true : false;

                    return $match;
                },
                'message' => 'ALPHANUM_SPACES',
        ]);

        $validator->scalar('lastname')
            ->notEmpty('lastname', 'LASTNAME_EMPTY')
            ->requirePresence('lastname', 'LASTNAME_REQUIRED')
            ->maxLength('lastname', 100, 'LASTNAME_MAXIMUM_LENGTH')
            ->minLength('lastname', 2, 'LASTNAME_MINIMUM_LENGTH')
            ->add('lastname', 'custom', [
                'rule' => function ($value) {
                    $match = preg_match('/^[a-zA-Z ]*$/i', $value) ? true : false;

                    return $match;
                },
                'message' => 'ALPHANUM_SPACES',
        ]);

        return $validator;
    }
}

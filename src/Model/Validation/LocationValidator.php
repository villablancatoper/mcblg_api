<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class LocationValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('location')
            ->maxLength('location', 150, 'LOCATION_MAXIMUM_LENGTH')
            ->add('location', 'custom', [
                'rule' => function ($value) {
                    return $match = preg_match('/^[a-zA-Z0-9 ]*$/i', $value) ? true : false;
                },
                'message' => 'ALPHANUM_SPACES',
        ]);

        return $validator;
    }
}

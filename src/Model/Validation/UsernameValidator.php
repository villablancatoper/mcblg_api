<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class UsernameValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('username')
            ->notEmpty('username', 'USERNAME_EMPTY')
            ->alphaNumeric('username', 'USERNAME_ALPHANUMERIC')
            ->requirePresence('username', 'USERNAME_REQUIRED')
            ->minLength('username', 5,'USERNAME_MINIMUM_LENGTH');

        return $validator;
    }
}

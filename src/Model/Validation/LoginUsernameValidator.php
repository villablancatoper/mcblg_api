<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class LoginUsernameValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('login_username')
            ->notEmpty('login_username', 'USERNAME_EMPTY')
            ->alphaNumeric('login_username', 'USERNAME_ALPHANUMERIC')
            ->requirePresence('login_username', 'USERNAME_REQUIRED');

        return $validator;
    }
}

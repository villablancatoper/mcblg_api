<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class PasswordValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->scalar('password')
            ->notEmpty('password', 'PASSWORD_EMPTY')
            ->alphaNumeric('password', 'PASSWORD_ALPHANUMERIC')
            ->requirePresence('password', 'PASSWORD_REQUIRED')
            ->maxLength('password', 100, 'PASSWORD_MAXIMUM_LENGTH')
            ->minLength('password', 5, 'PASSWORD_MINIMUM_LENGTH');

        return $validator;
    }
}

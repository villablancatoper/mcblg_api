<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class EmailValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->email('email')
            ->notEmpty('email', 'EMAIL_EMPTY')
            ->requirePresence('email', 'EMAIL_REQUIRED')
            ->maxLength('email', 100, 'EMAIL_MAXIMUM_LENGTH');

        return $validator;
    }
}

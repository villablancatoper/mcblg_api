<?php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class BirthdayValidator extends Validator
{
    /**
     * Construct Method
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * validationDefault Method
     *
     * @param Cake\Validation\Validator $validator instance of a validator
     * @return Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->notEmpty('birthday', 'BIRTHDAY_EMPTY')
        ->add('birthday', 'range', [
            'rule' => function ($value) {
                return date_create($value) >= date_create('1980-01-01') &&
                    $value < date('Y-m-d', strtotime('tomorrow'));
            },
            'message' => 'BIRTHDAY_INVALID_VALUE'
        ])
        ->add('birthday', 'custom', [
            'rule' => function ($value) {
                $birthday = date_create($value) ? date_create($value)->format('Y-m-d') : null;

                return $value == $birthday;
            },
            'message' => 'BIRTH_DATE_FORMAT_INCORRECT'
        ]);

        return $validator;
    }
}

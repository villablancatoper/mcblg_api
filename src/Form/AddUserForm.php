<?php
namespace App\Form;

use App\Model\Validation\UsernameValidator;
use App\Model\Validation\PasswordValidator;
use App\Model\Validation\ConfirmPasswordValidator;
use App\Model\Validation\NameValidator;
use App\Model\Validation\BirthdayValidator;
use App\Model\Validation\LocationValidator;
use App\Model\Validation\CityValidator;
use App\Model\Validation\EmailValidator;
use App\Model\Validation\ImageValidator;
use App\Model\Validation\BioValidator;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * AddUser Form.
 */
class AddUserForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {

        $usernameValidation = new UsernameValidator();
        $validator = $usernameValidation->validationDefault($validator);

        $passwordValidation = new PasswordValidator();
        $validator = $passwordValidation->validationDefault($validator);

        $confirmPasswordValidation = new ConfirmPasswordValidator();
        $validator = $confirmPasswordValidation->validationDefault($validator);

        $nameValidation = new NameValidator();
        $validator = $nameValidation->validationDefault($validator);

        $birthdayValidation = new BirthdayValidator();
        $validator = $birthdayValidation->validationDefault($validator);

        $locationValidation = new LocationValidator();
        $validator = $locationValidation->validationDefault($validator);
        $validator->allowEmpty('location');

        $cityValidation = new CityValidator();
        $validator = $cityValidation->validationDefault($validator);
        $validator->allowEmpty('city');

        $emailValidation = new EmailValidator();
        $validator = $emailValidation->validationDefault($validator);

        $imageValidation = new ImageValidator();
        $validator = $imageValidation->validationDefault($validator);
        $validator->allowEmpty('image');

        $bioValidation = new BioValidator();
        $validator = $bioValidation->validationDefault($validator);
        $validator->allowEmpty('bio');

        return $validator;
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }
}
